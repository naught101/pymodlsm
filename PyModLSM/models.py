# --------------------------------------------------------------------------- #
# PyModLSM - the Python Modular LSM
#
# Model classes.
# --------------------------------------------------------------------------- #

import numpy as np
import pandas as pd

from .component import ModelComponent
from . import science_functions as sf


class LinearModel(ModelComponent):

    def __init__(self,
                 params: 'Component parameter metadata, with defaults'={},
                 inputs: 'input variable metadata'={},
                 outputs: 'output variable metadata'={},
                 components: 'model sub-components'={},
                 states_init: "model states' initial conditions"={},
                 conserve: 'Whether the linear model should conserve energy and mass'=False):
        super().__init__(params=params, inputs=inputs, outputs=outputs, components=components, states_init=states_init)
        self.conserve = conserve
        # TODO: set initial, empty parameters? So we can build a model, and then request a list of required parameters, without fitting?

    # TODO: Abstract, so fit wrapper can be implemented in ModelComponent
    def fit(self, X, Y):
        self._fit_check_args(X=X, Y=Y)
        params = {}

        for y in Y:
            params[y] = self.linear_regression(X, Y[y])

        for component in self.components.values():
            component.fit(X, Y)
            params.update(component.get_params())

        self.params = params

    def linear_regression(self, X, y):
        # Ordinary least squares
        beta = np.linalg.inv(np.dot(X.T, X)).dot(np.dot(X.T, y))
        intercept = np.mean(np.dot(X, beta)) - np.mean(y)
        params = dict(zip(X.columns, beta))
        params.update({'intercept': intercept})
        return(params)

    def predict(self, X):
        self._predict_check_args(X=X)

        prediction = pd.DataFrame()
        for o in self.outputs:
            slopes = [self.params[o][i] for i in self.inputs]
            prediction[o] = np.dot(slopes, X[self.inputs].T) + self.params[o]['intercept']

        return(prediction)

# TODO: Distributed lag model: http://en.wikipedia.org/wiki/Distributed_lag
# Finite lag? 24 hours? Or exponential(-sinusoidal) infinite?


class SoilBucketModel(ModelComponent):
    """Budyko/Manabe soil moisture bucket model.

    Equations from Manabe (1969), after Budyko (1956).

    States:
     - Soil moisture

    References

    - Budyko, M.I., 1961. The heat balance of the earth’s surface. Soviet Geography, 2(4), pp.3–13. Available at: http://www.tandfonline.com/doi/abs/10.1080/00385417.1961.10770761 [Accessed January 27, 2015].
    - Manabe, S., 1969. Climate And The Ocean Circulation 1: I. The Atmospheric Circulation And The Hydrology Of The Earth’s Surface. Monthly Weather Review, 97(11), pp.739–774. Available at: http://journals.ametsoc.org/doi/abs/10.1175/1520-0493(1969)097%3C0739:CATOC%3E2.3.CO;2 [Accessed January 26, 2015].
    """

    defaults = {
        'params': {
            'soil water capacity': 0.5,    # m^3
            'air density': 0.6,            # kg m^-3
            'drag coefficient': 0.5,       # unitless
            'soil depth': 1,              # m
        },
        'inputs': {
            'Rainf': {
                'name': 'Rainfall',
                'units': 'mm/s',
            },
            'Wind': {
                'name': 'Wind speed',
                'units': 'm/s',
            },
            'Tair': {
                'name': 'Air Temperature',
                'units': 'K',
            },
            'Qair': {
                'name': 'Specific Humidity',
                'units': 'kg/kg',
            },
        },
        'outputs': {
            'Runoff': {
                'name': 'Run off',
                'units': 'kg/s',
            }
        },
        'states': {
        }
    }

    def __init__(self, states_init: "model states' initial conditions"={}):
        super().__init__(params=self.defaults['params'], inputs=self.defaults['inputs'], outputs=self.defaults['outputs'], states_init=states_init)

    def initialise(self):
        super().initialise()
        [self.params.setdefault(p, v) for p, v in self.defaults['params'].items()]

    def run(self, X):
        self._predict_check_args(X)

        results = [self.step(X.irow(t)) for t in range(len(X))]

        return pd.DataFrame.from_records(results)

    def step(self, x):
        result = pd.Series()

        # add rainfall
        soil_moist = self.states['soil moisture'] + x['Rainf']

        # calculate evaporation
        rho = self.params['air density']
        c_drag = self.params['drag coefficient']
        Qair_sat = sf.air_saturation_point(x['Tair'])

        evaporation = rho * c_drag * abs(x['Wind']) * (Qair_sat - x['Qair'])

        water_cap = self.params['soil water capacity'] * self.params['soil depth']

        if soil_moist < water_cap:
            evaporation = evaporation * soil_moist / water_cap

        result['evaporation'] = evaporation

        result['runoff'] = max(0, soil_moist - evaporation - water_cap)

        self.states['soil moisture'] = soil_moist - evaporation - result['runoff']

        return(result)
