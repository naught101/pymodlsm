# --------------------------------------------------------------------------- #
# PyModLSM - the Python Modular LSM
#
# Standard scientific constants.
# --------------------------------------------------------------------------- #

"""August-Roche-Magnus formula constants for calculating vapor saturation pressure from temperature.

Values from https://en.wikipedia.org/wiki/Clausius–Clapeyron_relation#Meteorology_and_climatology
"""
Magnus_a = 6.1094
Magnus_b = 17.625
Magnus_c = 243.04

"""Buck formula constants for calculating vapor saturation pressure from temperature and pressure.

Values from https://en.wikipedia.org/wiki/Relative_humidity#Measurement
"""
Buck_pa = 1.0007
Buck_pb = 3.46
Buck_a = 6.1121
Buck_b = 17.502
Buck_c = 240.97
