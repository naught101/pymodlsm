# --------------------------------------------------------------------------- #
# PyModLSM - the Python Modular LSM
#
# Standard scientific functions.
# --------------------------------------------------------------------------- #

import numpy as np

from . import constants as c


def air_saturation_point(Temp, Pressure=None):
    """Calculating vapor saturation pressure.

    If only temperature is given, use the August-Roche-Magnus formula, also commonly called the Tetens approximation, but see https://en.wikipedia.org/wiki/Clausius–Clapeyron_relation#Meteorology_and_climatology

    If temperature and pressure are given, then use the Buck formula.
    https://en.wikipedia.org/wiki/Relative_humidity#Measurement
    """

    if Pressure is not None:
        return ((c.Buck_pa + c.Buck_pb * Pressure)
                + c.Buck_a * np.exp((c.Buck_b * Temp) / (c.Buck_c + Temp)))
    else:
        return c.Magnus_a * np.exp((c.Magnus_b * Temp) / (c.Magnus_c + Temp))
