# --------------------------------------------------------------------------- #
# PyModLSM - the Python Modular LSM
#
# Data classes.
# --------------------------------------------------------------------------- #


class VariableBase:

    def __init__(self,
                 name: 'Variable name',
                 units: 'Units',  # TODO: might want to implement multiple variables, as per palsR
                 ):
        self.name = name
        self.units = units


class Variable(VariableBase):

    def __init__(self,
                 name: 'Variable name',
                 units: 'Units',
                 ):
        self.name = name
        self.units = units


class Parameter(VariableBase):

    def __init__(self,
                 name: 'Parameter name',
                 units: 'Units',
                 range: 'Parameter range',
                 default: 'Default parameter value',
                 ):
        self.name = name
        self.units = units
        self.range = range
        self.value = default

