# --------------------------------------------------------------------------- #
# PyModLSM - the Python Modular LSM
#
# Model component class
# --------------------------------------------------------------------------- #

import pandas as pd


class ModelComponent:
    """Generic model component.

    - includes a calculation routine, parameters.
    - takes met/prognostic variable input, flux/prognostic variables output
        - fits them with fit(X, Y)
        - predicts with predict(X)
        - runs the numerical model with run(X), or
        - steps over the numerical models with step(x)
    - required inputs, outputs, and default params and states in a nested dictionary.
    """

    defaults = {
        'params': {
        },
        'inputs': {
        },
        'outputs': {
        },
        'states': {
        }
    }

    def __init__(self,
                 params: 'Component parameter metadata, with defaults'={},
                 inputs: 'input variable metadata (required)'={},
                 outputs: 'output variable metadata (required)'={},
                 components: 'model sub-components'={},
                 states_init: "model states' initial conditions"={}
                 ):
        self.params = params
        self.components = components
        self.states = states_init
        self.inputs = inputs
        self.outputs = outputs
        self.initialise()

    def initialise(self):
        """helper function to create empty dicts, so model can be created from partial objects"""
        if not hasattr(self, 'params'):
            self.params = {}
        [self.params.setdefault(k, v) for (k, v) in self.defaults['params'].items()]

        if not hasattr(self, 'states'):
            self.states = {}
        [self.states.setdefault(k, v) for (k, v) in self.defaults['states'].items()]

        if not hasattr(self, 'inputs'):
            self.inputs = self.defaults['inputs']

        if not hasattr(self, 'outputs'):
            self.outputs = self.defaults['inputs']

        if not hasattr(self, 'components'):
            self.components = {}
        for component in self.components.values():
            component.initialise()

    def get_inputs(self):
        """return all inputs from this component and subcomponents"""
        # TODO: Should model components require the inputs of their submodels?
        # How do we handle components that calculate required inputs for their sub-components?
        inputs = {}
        for component in self.components.values():
            inputs.update(component.get_inputs())

        inputs.update(self.inputs)

        return inputs

    def get_outputs(self):
        """return all outputs from this component and subcomponents"""
        outputs = {}
        for component in self.components.values():
            outputs.update(component.get_outputs())

        outputs.update(self.outputs)
        return outputs

    def set_params(self, params):
        for component in self.components.values():
            component.set_params(params)

        for p in params:
            self.params[p] = params[p]

    def get_params(self):
        params = {}
        for component in self.components.values():
            params.update(component.get_params())
        params.update(self.params)
        return params

    def get_states(self):
        states = {}
        for component in self.components.items():
            states.update(component.get_states())
        states.update(self.states)
        return states

    def _fit_check_args(self, X, Y):
        assert isinstance(X, pd.DataFrame), "X is not a pandas DataFrame!"
        assert all([v in X.columns for v in self.get_inputs()]), \
            "Input data does not have all required variables"
        assert all([v in Y.columns for v in self.get_outputs()]), \
            "Output data does not have all required variables"

    def _predict_check_args(self, X):
        assert self.get_params() != {}, "Parameters not yet set"
        assert isinstance(X, pd.DataFrame), "X is not a pandas DataFrame!"
        assert all([v in X.columns for v in self.get_inputs()]), \
            "Input data does not have all required variables"

    def run(self, X):
        self._predict_check_args(X)

        results = pd.DataFrame()

        for component in self.components.values():
            res = component.run(X)
            results = results[results.columns - res.columns].join(res, how='outer')

        return results
