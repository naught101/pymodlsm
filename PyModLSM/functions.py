# --------------------------------------------------------------------------- #
# PyModLSM - the Python Modular LSM
#
# Data handling functions
# --------------------------------------------------------------------------- #

import yaml


def load_model(modelfile):
    """Loads a model, and puts it inside a handler, if it's not in one already"""

    from .handlers import ModelHandler

    yaml_stream = open(modelfile, 'r')
    model = yaml.load(yaml_stream)

    if not isinstance(model, ModelHandler):
        model = ModelHandler(components={'model': model})

    model.initialise()

    return model
