# --------------------------------------------------------------------------- #
# PyModLSM - the Python Modular LSM
#
# A modular model of land-surface processes.
# --------------------------------------------------------------------------- #

import sys

if sys.version_info[0] < 3:
    print("PyModLSM requires Python 3.x.")
    sys.exit()

from . import handlers
from . import models
from . import constants
from . import science_functions
from .functions import *
