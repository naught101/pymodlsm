# --------------------------------------------------------------------------- #
# PyModLSM - the Python Modular LSM
#
# Model handler class.
# --------------------------------------------------------------------------- #

import yaml
from .component import ModelComponent
import pandas as pd
import netCDF4


class ModelHandler(ModelComponent):
    """Generic model class that can be extended with components"""

    # TODO: abstract so that this can be used as an offline handler, as well as a coupled handler

    def __init__(self,
                 metadata: 'Model metadata'={},
                 params: 'Component parameter metadata, with defaults'={},
                 components: 'model sub-components'={},
                 inputs: 'input variable metadata'={},
                 outputs: 'output variable metadata'={},
                 states_init: "model states' initial conditions"={},
                 ):
        super().__init__(params=params, inputs=inputs, outputs=outputs, components=components, states_init=states_init)
        self.metadata = metadata

    def initialise(self):
        super().initialise()
        if not hasattr(self, 'metadata'):
            self.metadata = {}

    def set_metadata(self, name, data):
        self.metadata.update({name: data})

    def get_metadata(self, name=None):
        if name is None:
            return self.metadata
        else:
            return self.metadata[name]

    def save_params(self, filename):
        yaml.dump(self.get_params(), filename)

    def load_params(self, filename):
        f_params = open(filename, 'r')
        params = yaml.load(f_params)
        f_params.close()

        self.set_params(params)

    def load_data(self, filename, variables=None):
        """Loads data required by the model"""

        if variables is None:
            variables = self.get_inputs()

        # TODO: only works for 1D data so far
        nc = netCDF4.Dataset(filename)
        series = {}
        for v in variables:
            var_data = nc.variables[v]
            time = nc.variables['time']
            time = netCDF4.num2date(time[:], time.units)
            series.update({v: pd.Series(var_data[:].flatten(), index=time[:].flatten())})
        return(pd.DataFrame(series))

    def save_data(self, filename, data):
        """Saves data with model metadata"""

        nc = netCDF4.Dataset(filename, mode='w')
        nc.setncatts(self.get_metadata())
        nc.createDimension('time')
        for v in data:
            nc.createVariable(v, datatype='f8', dimensions='time')
            nc.variables[v][:] = data[v].as_matrix()
        nc.sync()
        nc.close()
