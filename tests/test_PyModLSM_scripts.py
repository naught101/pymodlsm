import unittest
import os
import sys
import glob


class TestModelScript(unittest.TestCase):

    def setUp(self):

        self.model_file = os.path.join('tests', 'data', 'BucketModel.yaml')
        self.met_file = os.path.join('tests', 'data', 'TumbaFluxnet.1.4_met_2002.nc')
        self.out_file = os.path.join('tests', 'out', 'TumbaFluxnet_prediction.nc')

    def test_script_run(self):
        from scripts.run_numerical_model import main

        self.assertNotIn('Hierarchical Data Format', os.popen('file %s' % self.out_file).read())

        main(self.model_file, self.met_file, self.out_file)

        output = sys.stdout.getvalue().strip()
        self.assertIn('Model output saved to {0}'.format(self.out_file), output)

        self.assertIn('Hierarchical Data Format', os.popen('file %s' % self.out_file).read())

    def tearDown(self):
        [os.remove(path) for path in glob.glob(os.path.join('tests', 'out', '*.yaml'))]
        [os.remove(path) for path in glob.glob(os.path.join('tests', 'out', '*.nc'))]


if __name__ == '__main__':
    unittest.main()
