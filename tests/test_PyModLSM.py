import unittest
import os
import glob
import yaml
import PyModLSM
import pandas as pd


class TestLinearModel(unittest.TestCase):

    def setUp(self):
        self.met_vars = {
            'SWdown': {},
            'Tair': {}
        }
        self.flux_vars = {
            'Qh': {},
            'Qle': {}
        }

        self.met_data = pd.DataFrame({'SWdown': [1, 2, 3, 4, 6],
                                      'Tair': [1, 2, 4, 8, 16]})
        self.flux_data = pd.DataFrame({'Qh': [3, 5, 2, 1, 5],
                                       'Qle': [1, 23, 7, 2, 6]})

        self.params = {'Qh':
                       {'SWdown': 1,
                        'Tair': 1,
                        'intercept': 0},
                       'Qle':
                       {'SWdown': 1,
                        'Tair': 1,
                        'intercept': 0},
                       }

        self.model = PyModLSM.models.LinearModel(inputs=self.met_vars,
                                                 outputs=self.flux_vars)

    def test_model_param_io(self):
        self.assertDictEqual(self.model.get_params(), {})
        self.model.set_params(self.params)
        self.assertDictEqual(self.model.get_params(), self.params)

    def test_model_fit(self):

        # Fit model
        self.model.fit(X=self.met_data, Y=self.flux_data)
        params = self.model.get_params()
        self.assertIsInstance(params, dict)
        self.assertGreater(len(params), 0)

        # Check parameters are correct
        [self.assertIn(v, params) for v in self.flux_vars]
        [self.assertIsInstance(params[p], dict) for p in params]
        [self.assertIn(v, params['Qh']) for v in self.met_vars]
        [self.assertIsInstance(c, float) for c in params['Qh'].values()]

        # TODO: Use values that make sense

    def test_model_predict(self):
        self.model.params = {}
        self.assertRaises(AssertionError, self.model.predict, self.met_data)

        # Load parameters
        self.model.set_params(self.params)

        # Run model
        prediction = self.model.predict(self.met_data)
        self.assertIsInstance(prediction, pd.DataFrame)
        self.assertEqual(prediction.shape, self.flux_data.shape)

        [self.assertEqual(list(prediction[v]), self.met_data.sum(axis=1).tolist()) for v in self.flux_vars]


class TestModelHandler(unittest.TestCase):

    def setUp(self):

        self.met_vars = ['SWdown', 'Tair']
        self.flux_vars = ['Qh', 'Qle']
        self.metadata = {'Model_name': "BasicLinearRegression"}

        self.flux_data = pd.DataFrame({'Qh': [3, 5, 2, 1, 5],
                                       'Qle': [1, 23, 7, 2, 6]})

        self.met_file = os.path.join('tests', 'data', 'TumbaFluxnet.1.4_met_2002.nc')
        self.flux_file = os.path.join('tests', 'data', 'TumbaFluxnet.1.4_flux_2002.nc')

        self.flux_out_file = os.path.join('tests', 'out', 'flux_prediction.nc')

        self.handler = PyModLSM.handlers.ModelHandler(metadata=self.metadata,
                                                      inputs=self.met_vars,
                                                      outputs=self.flux_vars)

    def test_load_data(self):
        # Load some Met data
        met_data = self.handler.load_data(filename=self.met_file, variables=self.met_vars)
        # Test that data is viable (Pandas format?)
        self.assertIsInstance(met_data, pd.DataFrame)
        [self.assertIn(v, met_data.columns) for v in self.met_vars]

    def test_save_flux(self):
        # Save flux prediction
        self.handler.save_data(filename=self.flux_out_file, data=self.flux_data)

        self.assertTrue(os.path.isfile(self.flux_out_file))
        self.assertIn('Hierarchical Data Format', os.popen('file %s' % self.flux_out_file).read())

    def tearDown(self):
        [os.remove(path) for path in glob.glob(os.path.join('PyModLSM', 'tests', 'out', '*.yaml'))]
        [os.remove(path) for path in glob.glob(os.path.join('PyModLSM', 'tests', 'out', '*.nc'))]


class TestModelLoading(unittest.TestCase):

    def setUp(self):

        self.model_file = os.path.join('tests', 'data', 'LinearModel.yaml')

    def test_model_load_initialise(self):
        f = open(self.model_file, 'r')

        model = yaml.load(f)

        self.assertRaises(AttributeError, model.get_params)

        model.initialise()

        inputs = model.get_inputs()
        outputs = model.get_outputs()

        self.assertListEqual(sorted(inputs.keys()), ['SWdown', 'Tair'])
        self.assertListEqual(sorted(outputs.keys()), ['Qh', 'Qle'])


if __name__ == '__main__':
    unittest.main()
