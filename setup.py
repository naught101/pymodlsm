#!/usr/bin/env python

from distutils.core import setup

setup(
    name='PyModLSM',
    version='0.01',
    description='Python Modular Land Surface Model',
    long_description=open('README.md').read(),
    author='ned haughton',
    author_email='ned@nedhaughton.com',
    url='https://bitbucket.org/naught101/pymodlsm',
    packages=['PyModLSM'],
)
