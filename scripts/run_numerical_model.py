#!/usr/bin/env python3

# creates sim links from PALS model output .nc files to readable file names.

import argparse
import PyModLSM


def main(modelfile, metfile, outfile):

    model = PyModLSM.load_model(modelfile)
    model.initialise()

    met_data = model.load_data(metfile)

    results = model.run(met_data)

    model.save_data(outfile, results)

    print('Model output saved to {0}'.format(outfile))

if (__name__ == '__main__'):
    # Script arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('modelfile', help='path to YAML file that describes a PyModLSM model.')
    parser.add_argument('metfile', help='path to .nc file that contains the met data required by the model.')
    parser.add_argument('outfile', help='path where flux data .nc file should be saved.')

    args = parser.parse_args()

    main(args.modelfile, args.metfile, args.outfile)
