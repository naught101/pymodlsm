Notes
-----
### Make modular

It would be good to have a model that allows alternate structures.
Ideally, it would be good to have the ability to implement arbitrary structures.

A model structure consists of:

- Model Inputs
    - Met data
    - Metadata (location, timescale)
- Model components, each of which transform the model inputs
    - Each component requires parameters.
        - Shared parameters? Global parameters?
        - Default parameters.
    - Each component can have different dependencies (e.g. prognostic variables?)
        - Dependencies are component inputs. Variable flow is controlled by the model (directed graph)
    - There needs to be stub components, or default components, that do the least possible work to output the minimum variables
    - Components run sequentially? Concurrently (if not dependent)?
    - Sequence is defined by directed graph. Fail on inappropriate order (unmet dependencies)?
- Model states
    - Initial conditions
    - Held by the model. Components can implement their own internal states, if necessary.
- Model outputs.
    - per-timestep

## Questions

- How do we deal with model calibration?
    - Fit per-component? Components (esp. empirical models) could implement their own fit functions.
    - Normal calibration (parameters should include ranges)?
- parameters and variables as objects? Allows better tracking of metadata. Less efficient?
- rip the guts out of CABLE as components.

- Ask Martyn Clark for details on his LSM follow-up to FUSE.
    - What features will it include?
    - Will it eventually be open-sourced?
    - Same questions for FUSE.

### Ideas

- Non-constant time-intervals? Does it make sense to have shorter timesteps during certain times of the day? (dawn/dusk?)
- Keep in mind the possibility of creating a UI for the model that allows the user to see what's going on in real-time. Could be an excellent educational tool.
- Make models/components as self-documenting as possible.
