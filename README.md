PyModLSM
========

Python Modular Land Surface Model.

This model is designed for exploring land surface model structures, and identifying different behaviours that arise from those structures.
It is *not* intended to be efficient, and nor is it intended to be an accurate model for prediction purposes.

